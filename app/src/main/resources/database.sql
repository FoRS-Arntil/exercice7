CREATE TABLE Voiture (
    id INT AUTO_INCREMENT PRIMARY KEY,
    marque VARCHAR(255),
    modele VARCHAR(255),
    cylindree DOUBLE
);

CREATE TABLE Moteur (
    id INT AUTO_INCREMENT PRIMARY KEY,
    nom VARCHAR(255),
    voitureId INT,
    FOREIGN KEY (voitureId) REFERENCES Voiture(id)
)