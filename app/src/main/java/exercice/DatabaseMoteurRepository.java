package exercice;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import exercice.exceptions.RepositoryException;

public class DatabaseMoteurRepository extends DatabaseDataMapper<Moteur> implements MoteurRepository {
    public static String tableName = "Moteur";
    private static DatabaseMoteurRepository instance;

    public static DatabaseMoteurRepository getInstance(String connectionString) throws RepositoryException {
        if (instance == null) {
            instance = new DatabaseMoteurRepository(connectionString);
        }
        return instance;
    }

    private DatabaseMoteurRepository(String connectionString) throws RepositoryException {
        super(connectionString, Moteur.class, DatabaseMoteurRepository.tableName);
    }
    
    public static Moteur mapRow(ResultSet rs) {
        try {
            return new Moteur(rs.getString("nom"));
        }
        catch(SQLException e) {
            e.printStackTrace();
        }
        return null;
    }


    @Override
    public List<Moteur> getMoteursForVoiture(Voiture voiture) throws RepositoryException {
        List<Moteur> moteurs = new ArrayList<>();
        try (Connection connection = DriverManager.getConnection(connectionString)) {
            int voitureId = DatabaseVoitureRepository.getInstance(connectionString).getId(voiture);
            String sql = "SELECT * FROM Moteur WHERE voitureId = ?";
            try (PreparedStatement statement = connection.prepareStatement(sql)) {
                statement.setInt(1, voitureId);
                try (ResultSet resultSet = statement.executeQuery()) {
                    while (resultSet.next()) {
                        moteurs.add(DatabaseMoteurRepository.mapRow(resultSet));
                    }
                }
            }
        } catch (SQLException e) {
            throw new RepositoryException("Erreur SQL", e);
        }

        return moteurs;
    }

    protected void associerVoiture(Moteur moteur, int voitureId, Connection connection) throws RepositoryException {
        int moteurId = this.getId(moteur);
        String sql = "UPDATE " + DatabaseMoteurRepository.tableName + " SET voitureId = ? WHERE id = ?";
        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setInt(1, voitureId);
            statement.setInt(2, moteurId);
            statement.executeUpdate();
        }
        catch (SQLException e) {
            throw new RepositoryException("Erreur SQL", e);
        }
    }
}
