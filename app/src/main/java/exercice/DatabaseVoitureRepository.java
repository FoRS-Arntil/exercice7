package exercice;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import exercice.exceptions.RepositoryException;

public class DatabaseVoitureRepository extends DatabaseDataMapper<Voiture> implements VoitureRepository{
    public static String tableName = "Voiture";
    private static DatabaseVoitureRepository instance;

    public static DatabaseVoitureRepository getInstance(String connectionString) throws RepositoryException {
        if (instance == null) {
            instance = new DatabaseVoitureRepository(connectionString);
        }
        return instance;
    }

    private DatabaseVoitureRepository(String connectionString) throws RepositoryException {
        super(connectionString, Voiture.class, DatabaseVoitureRepository.tableName);
    }

    @Override
    public Voiture getByModele(String modele) {
        try (Connection connection = DriverManager.getConnection(connectionString)) {
            String sql = "SELECT * FROM "+ tableName +" WHERE modele = ? LIMIT 1";
            try (PreparedStatement statement = connection.prepareStatement(sql)) {
                statement.setString(1, modele);
                try (ResultSet resultSet = statement.executeQuery()) {
                    if (resultSet.next()) {
                        return mapRow(resultSet);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace(); // Gestion appropriée des exceptions dans un environnement réel
        }

        return null;
    }

    public static Voiture mapRow(ResultSet rs) {
        try {
            return new Voiture(rs.getString("marque"), rs.getString("modele"), rs.getDouble("cylindree"));
        }
        catch(SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void saveVoitureMoteurs(Voiture voiture, List<Moteur> moteurs) throws RepositoryException{
        try (Connection connection = DriverManager.getConnection(connectionString)) {
            connection.setAutoCommit(false);
            save(voiture);
            int voitureId = this.getId(voiture);
            DatabaseMoteurRepository dmr = DatabaseMoteurRepository.getInstance(connectionString);
            for (Moteur moteur : moteurs) {
                dmr.save(moteur, connection);
                dmr.associerVoiture(moteur, voitureId, connection);
            }
            connection.commit();
        } catch (SQLException e) {
            // Rollback automatique car la connection est fermée sans faire de commit
            // Attention peut poser des problèmes pour certains SGBD, a vous de trouver une solution si ça arrive
            throw new RepositoryException("Problème SQL", e);
        }
    }
}
