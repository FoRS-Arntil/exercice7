package exercice;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.Map;
import java.util.WeakHashMap;

import exercice.exceptions.RepositoryException;

public class DatabaseDataMapper<T> implements DatabaseIdentityMapper<T> {
    protected final String connectionString;
    protected final String tableName;
    private final Class<T> entityClass;
    private Map<T, Integer> objRefMap = new WeakHashMap<T, Integer>();

    public DatabaseDataMapper(String connectionString, Class<T> entityClass, String tableName) throws RepositoryException {
        this.connectionString = connectionString;
        this.entityClass = entityClass;
        this.tableName = tableName;
    }

    @Override
    public List<T> getAll() {
        // TODO Auto-generated method stub
        throw new UnsupportedOperationException("Unimplemented method 'getAll'");
    }

    @Override
    public void save(T entity) throws RepositoryException{
        Field[] fields = getAllFields(entityClass);
        String insertQuery = buildSaveQuery(fields);
        try (Connection connection = DriverManager.getConnection(connectionString)) {
            int id = saveStatement(connection, entity, fields, insertQuery);
            objRefMap.put(entity, id);
        }
        catch (SQLException e) {
            throw new RepositoryException("SQL exception", e);
        }
    }

    protected void save(T entity, Connection connection) throws RepositoryException {
        Field[] fields = getAllFields(entityClass);
        String insertQuery = buildSaveQuery(fields);
        int id = saveStatement(connection, entity, fields, insertQuery);
        objRefMap.put(entity, id);
    }

    private String buildSaveQuery(Field[] fields) {
        StringBuilder columns = new StringBuilder();
        StringBuilder values = new StringBuilder();

        for (Field field : fields) {
            field.setAccessible(true);
            String columnName = field.getName();
            columns.append(columnName).append(", ");
            values.append("?, ");
        }

        // Supprimer la virgule en trop à la fin des clauses
        columns.delete(columns.length() - 2, columns.length());
        values.delete(values.length() - 2, values.length());

        return "INSERT INTO " + tableName + " (" + columns.toString() + ") VALUES (" + values.toString() + ")";
    }

    private int saveStatement(Connection connection, T entity, Field[] fields, String insertQuery) throws RepositoryException {
        try (PreparedStatement preparedStatement = connection.prepareStatement(insertQuery, Statement.RETURN_GENERATED_KEYS)) {
            int parameterIndex = 1;

            // Définir les valeurs des paramètres dans la clause VALUES
            for (Field field : fields) {
                field.setAccessible(true);
                Object columnValue = field.get(entity);
                preparedStatement.setObject(parameterIndex++, columnValue);
            }

            // Exécuter l'insertion
            preparedStatement.execute();

            try (ResultSet generatedKeys = preparedStatement.getGeneratedKeys()) {
                if (generatedKeys.next()) {
                    return generatedKeys.getInt(1);
                } else {
                    throw new SQLException("Aucune clé générée après l'insertion.");
                }
            }
        }
        catch (SQLException e) {
            throw new RepositoryException("Erreur SQL", e);
        }
        catch (IllegalAccessException e) {
            throw new RepositoryException("Impossible d'accéder au membre", e);
        }
    }

    @Override
    public void update(T entity) {
        // TODO Auto-generated method stub
        throw new UnsupportedOperationException("Unimplemented method 'update'");
    }

    @Override
    public void delete(T entity) {
        // TODO Auto-generated method stub
        throw new UnsupportedOperationException("Unimplemented method 'delete'");
    }

    @Override
    public T findById(int id) throws RepositoryException {
        try (Connection connection = DriverManager.getConnection(connectionString)) {
            Field[] fields = getAllFields(entityClass);
            String whereClause = "id = ?";
            String selectQuery = "SELECT * FROM " + tableName + " WHERE " + whereClause;

            try (PreparedStatement preparedStatement = connection.prepareStatement(selectQuery)) {
                preparedStatement.setInt(1, id);
                try (ResultSet resultSet = preparedStatement.executeQuery()) {
                    if (resultSet.next()) {
                        T entity = entityClass.getDeclaredConstructor().newInstance();
                        for (Field field : fields) {
                            field.setAccessible(true);
                            String columnName = field.getName();
                            Object columnValue = resultSet.getObject(columnName);
                            field.set(entity, columnValue);
                        }

                        return entity;
                    } else {
                        return null;  // Aucun résultat trouvé pour l'id donné
                    }
                }
            }
        } catch (InstantiationException | IllegalAccessException | IllegalArgumentException
                                | InvocationTargetException | NoSuchMethodException | SecurityException e) {
            throw new RepositoryException("Pas de constructeur par défaut", e);
        } catch (SQLException e) {
            throw new RepositoryException("Erreur SQL", e);
        }
    }

    private Field[] getAllFields(Class<?> clazz) {
        // Collecter tous les champs de la classe courante et de ses superclasses
        java.util.List<Field> fields = new java.util.ArrayList<>();
        while (clazz != null) {
            fields.addAll(java.util.Arrays.asList(clazz.getDeclaredFields()));
            clazz = clazz.getSuperclass();
        }
        return fields.toArray(new Field[0]);
    }

    protected int getId(T entity) {
        return objRefMap.get(entity);
    }
}
