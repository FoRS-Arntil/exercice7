package exercice;

import java.util.List;

import exercice.exceptions.RepositoryException;

public interface MoteurRepository extends DataMapper<Moteur>{
    List<Moteur> getMoteursForVoiture(Voiture voiture) throws RepositoryException;
}
