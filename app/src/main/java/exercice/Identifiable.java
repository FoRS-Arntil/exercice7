package exercice;

public interface Identifiable {
    int getId();
    void setId(int id);

    String getTableName();
}
