package exercice;

import java.util.List;

import exercice.exceptions.RepositoryException;

public interface DataMapper<T> {
    List<T> getAll();
    void save(T entity) throws RepositoryException;
    void update(T entity);
    void delete(T entity);
}