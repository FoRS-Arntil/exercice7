package exercice;

public class Voiture {
    private String marque;
    private String modele;
    private double cylindree;

    public Voiture(String marque, String modele, double cylindree) {
        this.marque = marque;
        this.modele = modele;
        this.cylindree = cylindree;
    }

    public String getMarque() {
        return marque;
    }

    public void setMarque(String marque) {
        this.marque = marque;
    }

    public String getModele() {
        return modele;
    }

    public void setModele(String modele) {
        this.modele = modele;
    }

    public double getCylindree() {
        return cylindree;
    }

    public void setCylindree(float cylindree) {
        this.cylindree = cylindree;
    }
}
