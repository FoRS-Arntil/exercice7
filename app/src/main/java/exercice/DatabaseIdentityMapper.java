
package exercice;

import exercice.exceptions.RepositoryException;

public interface DatabaseIdentityMapper<T> extends DataMapper<T> {
    T findById(int id) throws RepositoryException;  
}