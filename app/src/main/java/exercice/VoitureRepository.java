package exercice;

import java.util.List;

import exercice.exceptions.RepositoryException;

public interface VoitureRepository extends DataMapper<Voiture>{
    Voiture getByModele(String modele);
    void saveVoitureMoteurs(Voiture voiture, List<Moteur> moteurs) throws RepositoryException;
}
